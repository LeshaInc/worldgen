#version 450

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec2 inInstPos;
layout(location = 2) in vec3 inInstColor;

layout(location = 0) out vec3 fColor;

void main() {
    fColor = inInstColor;
    gl_Position = vec4(inPos + vec3(inInstPos, 0.0), 1.0);
}
