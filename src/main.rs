use std::cell::RefCell;
use std::io::Cursor;
use std::rc::Rc;

#[repr(C)]
#[derive(Clone, Copy)]
struct VoronoiVertex {
    pos: [f32; 3],
}

#[repr(C)]
#[derive(Clone, Copy)]
struct VoronoiInstance {
    pos: [f32; 2],
    color: [f32; 3],
}

fn create_voronoi_cylinder() -> (Vec<VoronoiVertex>, Vec<u16>) {
    const SIDES: usize = 8;
    const RADIUS: f32 = std::f32::consts::SQRT_2;

    let mut vertices = Vec::with_capacity(SIDES + 1);
    let mut indices = Vec::with_capacity(SIDES * 3);

    let (sin, cos) = (360.0 / (SIDES as f32)).to_radians().sin_cos();
    let mut x = 0.0;
    let mut y = RADIUS;

    vertices.push(VoronoiVertex {
        pos: [0.0, 0.0, 0.0],
    });

    for i in 0..SIDES {
        vertices.push(VoronoiVertex { pos: [x, y, 1.0] });
        indices.push(0);
        indices.push((i + 1) as u16);
        indices.push((1 + ((i + 1) % SIDES)) as u16);

        let nx = cos * x - sin * y;
        let ny = sin * x + cos * y;
        x = nx;
        y = ny;
    }

    (vertices, indices)
}

#[repr(C)]
#[derive(Clone, Copy)]
struct ProcessingVertex {
    pos: [f32; 2],
    uv: [f32; 2],
}

fn create_processing_rect() -> Vec<ProcessingVertex> {
    [
        ProcessingVertex {
            pos: [-1.0, 1.0],
            uv: [0.0, 0.0],
        },
        ProcessingVertex {
            pos: [1.0, 1.0],
            uv: [1.0, 0.0],
        },
        ProcessingVertex {
            pos: [1.0, -1.0],
            uv: [1.0, 1.0],
        },
        ProcessingVertex {
            pos: [-1.0, 1.0],
            uv: [0.0, 0.0],
        },
        ProcessingVertex {
            pos: [1.0, -1.0],
            uv: [1.0, 1.0],
        },
        ProcessingVertex {
            pos: [-1.0, -1.0],
            uv: [0.0, 1.0],
        },
    ]
    .to_vec()
}

fn color_texture_desc() -> wgpu::TextureDescriptor {
    wgpu::TextureDescriptor {
        size: wgpu::Extent3d {
            width: RESOLUTION,
            height: RESOLUTION,
            depth: 1,
        },
        array_layer_count: 1,
        mip_level_count: 1,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
        format: wgpu::TextureFormat::Rgba8Unorm,
        usage: wgpu::TextureUsage::SAMPLED
            | wgpu::TextureUsage::OUTPUT_ATTACHMENT
            | wgpu::TextureUsage::COPY_SRC,
    }
}

fn create_color_texture(device: &wgpu::Device) -> wgpu::Texture {
    device.create_texture(&color_texture_desc())
}

fn create_depth_texture(device: &wgpu::Device) -> wgpu::Texture {
    let desc = wgpu::TextureDescriptor {
        format: wgpu::TextureFormat::Depth32Float,
        usage: wgpu::TextureUsage::OUTPUT_ATTACHMENT,
        ..color_texture_desc()
    };
    device.create_texture(&desc)
}

struct VoronoiPipeline {
    pipeline: wgpu::RenderPipeline,
    vertex_buffer: wgpu::Buffer,
    index_buffer: wgpu::Buffer,
    color_texture_view: wgpu::TextureView,
    depth_texture_view: wgpu::TextureView,
    num_indices: u32,
}

const RESOLUTION: u32 = 512;

impl VoronoiPipeline {
    fn new(device: &wgpu::Device) -> VoronoiPipeline {
        let source = include_bytes!("../shader/voronoi.vert.spv");
        let spirv = wgpu::read_spirv(Cursor::new(&source[..])).unwrap();
        let vs_module = device.create_shader_module(&spirv);

        let source = include_bytes!("../shader/voronoi.frag.spv");
        let spirv = wgpu::read_spirv(Cursor::new(&source[..])).unwrap();
        let fs_module = device.create_shader_module(&spirv);

        let desc = wgpu::PipelineLayoutDescriptor {
            bind_group_layouts: &[],
        };
        let pipeline_layout = device.create_pipeline_layout(&desc);

        let desc = wgpu::RenderPipelineDescriptor {
            layout: &pipeline_layout,
            vertex_stage: wgpu::ProgrammableStageDescriptor {
                module: &vs_module,
                entry_point: "main",
            },
            fragment_stage: Some(wgpu::ProgrammableStageDescriptor {
                module: &fs_module,
                entry_point: "main",
            }),
            rasterization_state: Some(Default::default()),
            primitive_topology: wgpu::PrimitiveTopology::TriangleList,
            color_states: &[wgpu::ColorStateDescriptor {
                format: wgpu::TextureFormat::Rgba8Unorm,
                alpha_blend: wgpu::BlendDescriptor::REPLACE,
                color_blend: wgpu::BlendDescriptor::REPLACE,
                write_mask: wgpu::ColorWrite::ALL,
            }],
            depth_stencil_state: Some(wgpu::DepthStencilStateDescriptor {
                format: wgpu::TextureFormat::Depth32Float,
                depth_write_enabled: true,
                depth_compare: wgpu::CompareFunction::LessEqual,
                stencil_front: wgpu::StencilStateFaceDescriptor::IGNORE,
                stencil_back: wgpu::StencilStateFaceDescriptor::IGNORE,
                stencil_read_mask: 0,
                stencil_write_mask: 0,
            }),
            index_format: wgpu::IndexFormat::Uint16,
            vertex_buffers: &[
                wgpu::VertexBufferDescriptor {
                    stride: 12,
                    step_mode: wgpu::InputStepMode::Vertex,
                    attributes: &[wgpu::VertexAttributeDescriptor {
                        offset: 0,
                        format: wgpu::VertexFormat::Float3,
                        shader_location: 0,
                    }],
                },
                wgpu::VertexBufferDescriptor {
                    stride: 20,
                    step_mode: wgpu::InputStepMode::Instance,
                    attributes: &[
                        wgpu::VertexAttributeDescriptor {
                            offset: 0,
                            format: wgpu::VertexFormat::Float2,
                            shader_location: 1,
                        },
                        wgpu::VertexAttributeDescriptor {
                            offset: 8,
                            format: wgpu::VertexFormat::Float3,
                            shader_location: 2,
                        },
                    ],
                },
            ],
            sample_count: 1,
            sample_mask: !0,
            alpha_to_coverage_enabled: false,
        };
        let pipeline = device.create_render_pipeline(&desc);

        let (vertices, indices) = create_voronoi_cylinder();

        let vertex_buffer = device
            .create_buffer_mapped(vertices.len(), wgpu::BufferUsage::VERTEX)
            .fill_from_slice(&vertices);

        let index_buffer = device
            .create_buffer_mapped(indices.len(), wgpu::BufferUsage::INDEX)
            .fill_from_slice(&indices);

        let color_texture_view = create_color_texture(device).create_default_view();
        let depth_texture_view = create_depth_texture(device).create_default_view();

        VoronoiPipeline {
            pipeline,
            vertex_buffer,
            index_buffer,
            color_texture_view,
            depth_texture_view,
            num_indices: indices.len() as u32,
        }
    }

    fn run(
        &self,
        device: &mut wgpu::Device,
        encoder: &mut wgpu::CommandEncoder,
        instances: &[VoronoiInstance],
    ) {
        let instance_buffer = device
            .create_buffer_mapped(instances.len(), wgpu::BufferUsage::VERTEX)
            .fill_from_slice(&instances);

        let desc = wgpu::RenderPassDescriptor {
            color_attachments: &[wgpu::RenderPassColorAttachmentDescriptor {
                attachment: &self.color_texture_view,
                resolve_target: None,
                load_op: wgpu::LoadOp::Clear,
                store_op: wgpu::StoreOp::Store,
                clear_color: wgpu::Color {
                    r: 0.1,
                    g: 0.2,
                    b: 0.3,
                    a: 1.0,
                },
            }],
            depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachmentDescriptor {
                attachment: &self.depth_texture_view,
                depth_load_op: wgpu::LoadOp::Clear,
                depth_store_op: wgpu::StoreOp::Store,
                stencil_load_op: wgpu::LoadOp::Clear,
                stencil_store_op: wgpu::StoreOp::Store,
                clear_depth: 1.0,
                clear_stencil: 0,
            }),
        };

        let mut pass = encoder.begin_render_pass(&desc);
        pass.set_pipeline(&self.pipeline);
        pass.set_index_buffer(&self.index_buffer, 0);
        pass.set_vertex_buffers(0, &[(&self.vertex_buffer, 0), (&instance_buffer, 0)]);
        pass.draw_indexed(0..self.num_indices, 0, 0..(instances.len() as u32));
    }
}

struct DistortionPipeline {
    pipeline: wgpu::RenderPipeline,
    bindings: wgpu::BindGroup,
    vertex_buffer: wgpu::Buffer,
    color_texture: wgpu::Texture,
    color_texture_view: wgpu::TextureView,
}

impl DistortionPipeline {
    fn new(device: &wgpu::Device, input: &wgpu::TextureView) -> DistortionPipeline {
        let source = include_bytes!("../shader/distortion.vert.spv");
        let spirv = wgpu::read_spirv(Cursor::new(&source[..])).unwrap();
        let vs_module = device.create_shader_module(&spirv);

        let source = include_bytes!("../shader/distortion.frag.spv");
        let spirv = wgpu::read_spirv(Cursor::new(&source[..])).unwrap();
        let fs_module = device.create_shader_module(&spirv);

        let desc = wgpu::SamplerDescriptor {
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Nearest,
            min_filter: wgpu::FilterMode::Nearest,
            mipmap_filter: wgpu::FilterMode::Nearest,
            lod_min_clamp: -100.0,
            lod_max_clamp: 100.0,
            compare_function: wgpu::CompareFunction::LessEqual,
        };
        let sampler = device.create_sampler(&desc);

        let desc = wgpu::BindGroupLayoutDescriptor {
            bindings: &[
                wgpu::BindGroupLayoutBinding {
                    binding: 0,
                    visibility: wgpu::ShaderStage::FRAGMENT,
                    ty: wgpu::BindingType::SampledTexture {
                        multisampled: false,
                        dimension: wgpu::TextureViewDimension::D2,
                    },
                },
                wgpu::BindGroupLayoutBinding {
                    binding: 1,
                    visibility: wgpu::ShaderStage::FRAGMENT,
                    ty: wgpu::BindingType::Sampler,
                },
            ],
        };
        let bind_group_layout = device.create_bind_group_layout(&desc);

        let desc = wgpu::BindGroupDescriptor {
            layout: &bind_group_layout,
            bindings: &[
                wgpu::Binding {
                    binding: 0,
                    resource: wgpu::BindingResource::TextureView(input),
                },
                wgpu::Binding {
                    binding: 1,
                    resource: wgpu::BindingResource::Sampler(&sampler),
                },
            ],
        };
        let bind_group = device.create_bind_group(&desc);

        let desc = wgpu::PipelineLayoutDescriptor {
            bind_group_layouts: &[&bind_group_layout],
        };
        let pipeline_layout = device.create_pipeline_layout(&desc);

        let desc = wgpu::RenderPipelineDescriptor {
            layout: &pipeline_layout,
            vertex_stage: wgpu::ProgrammableStageDescriptor {
                module: &vs_module,
                entry_point: "main",
            },
            fragment_stage: Some(wgpu::ProgrammableStageDescriptor {
                module: &fs_module,
                entry_point: "main",
            }),
            rasterization_state: Some(Default::default()),
            primitive_topology: wgpu::PrimitiveTopology::TriangleList,
            color_states: &[wgpu::ColorStateDescriptor {
                format: wgpu::TextureFormat::Rgba8Unorm,
                alpha_blend: wgpu::BlendDescriptor::REPLACE,
                color_blend: wgpu::BlendDescriptor::REPLACE,
                write_mask: wgpu::ColorWrite::ALL,
            }],
            depth_stencil_state: None,
            index_format: wgpu::IndexFormat::Uint16,
            vertex_buffers: &[wgpu::VertexBufferDescriptor {
                stride: 16,
                step_mode: wgpu::InputStepMode::Vertex,
                attributes: &[
                    wgpu::VertexAttributeDescriptor {
                        offset: 0,
                        format: wgpu::VertexFormat::Float2,
                        shader_location: 0,
                    },
                    wgpu::VertexAttributeDescriptor {
                        offset: 8,
                        format: wgpu::VertexFormat::Float2,
                        shader_location: 1,
                    },
                ],
            }],
            sample_count: 1,
            sample_mask: !0,
            alpha_to_coverage_enabled: false,
        };
        let pipeline = device.create_render_pipeline(&desc);

        let vertices = create_processing_rect();

        let vertex_buffer = device
            .create_buffer_mapped(vertices.len(), wgpu::BufferUsage::VERTEX)
            .fill_from_slice(&vertices);

        let color_texture = create_color_texture(device);
        let color_texture_view = color_texture.create_default_view();

        DistortionPipeline {
            pipeline,
            bindings: bind_group,
            vertex_buffer,
            color_texture,
            color_texture_view,
        }
    }

    fn run(&self, encoder: &mut wgpu::CommandEncoder) {
        let desc = wgpu::RenderPassDescriptor {
            color_attachments: &[wgpu::RenderPassColorAttachmentDescriptor {
                attachment: &self.color_texture_view,
                resolve_target: None,
                load_op: wgpu::LoadOp::Clear,
                store_op: wgpu::StoreOp::Store,
                clear_color: wgpu::Color {
                    r: 0.1,
                    g: 0.2,
                    b: 0.3,
                    a: 1.0,
                },
            }],
            depth_stencil_attachment: None,
        };

        let mut pass = encoder.begin_render_pass(&desc);
        pass.set_bind_group(0, &self.bindings, &[]);
        pass.set_pipeline(&self.pipeline);
        pass.set_vertex_buffers(0, &[(&self.vertex_buffer, 0)]);
        pass.draw(0..6, 0..1);
    }
}

fn gen_random_points(count: usize) -> Vec<VoronoiInstance> {
    use rand::Rng;

    let mut rng = rand::thread_rng();

    (0..count)
        .map(|_| VoronoiInstance {
            pos: [rng.gen_range(-1.0, 1.0), rng.gen_range(-1.0, 1.0)],
            color: rng.gen(),
        })
        .collect::<Vec<_>>()
}

struct DistortedVoronoiPipeline {
    voronoi: VoronoiPipeline,
    distortion: DistortionPipeline,
    download_buffer: wgpu::Buffer,
}

impl DistortedVoronoiPipeline {
    fn new(device: &wgpu::Device) -> DistortedVoronoiPipeline {
        let voronoi = VoronoiPipeline::new(device);
        let distortion = DistortionPipeline::new(device, &voronoi.color_texture_view);

        let desc = wgpu::BufferDescriptor {
            size: u64::from(RESOLUTION) * u64::from(RESOLUTION) * 4,
            usage: wgpu::BufferUsage::MAP_READ | wgpu::BufferUsage::COPY_DST,
        };
        let download_buffer = device.create_buffer(&desc);

        DistortedVoronoiPipeline {
            voronoi,
            distortion,
            download_buffer,
        }
    }

    fn run(&self, device: &mut wgpu::Device, points: &[VoronoiInstance]) -> Vec<u8> {
        let desc = wgpu::CommandEncoderDescriptor { todo: 0 };
        let mut encoder = device.create_command_encoder(&desc);

        self.voronoi.run(device, &mut encoder, points);
        self.distortion.run(&mut encoder);

        let src = wgpu::TextureCopyView {
            texture: &self.distortion.color_texture,
            mip_level: 0,
            array_layer: 0,
            origin: wgpu::Origin3d::ZERO,
        };

        let dst = wgpu::BufferCopyView {
            buffer: &self.download_buffer,
            offset: 0,
            row_pitch: 4 * RESOLUTION,
            image_height: RESOLUTION,
        };

        encoder.copy_texture_to_buffer(
            src,
            dst,
            wgpu::Extent3d {
                width: RESOLUTION,
                height: RESOLUTION,
                depth: 1,
            },
        );

        let mut queue = device.get_queue();
        let command_buffer = encoder.finish();
        queue.submit(&[command_buffer]);

        let size = u64::from(RESOLUTION) * u64::from(RESOLUTION) * 4;
        let data = Rc::new(RefCell::new(Vec::<u8>::new()));

        let data_1 = data.clone();
        self.download_buffer.map_read_async(0, size, move |result| {
            *data_1.borrow_mut() = result.unwrap().data.to_vec();
        });

        device.poll(true);
        data.replace(Vec::new())
    }
}

fn main() {
    let instance = wgpu::Instance::new();
    let adapter = instance.request_adapter(&Default::default());
    let mut device = adapter.request_device(&Default::default());

    let pipeline = DistortedVoronoiPipeline::new(&device);

    let points = gen_random_points(25);
    let data = pipeline.run(&mut device, &points);

    image::save_buffer(
        "result.png",
        &data,
        RESOLUTION,
        RESOLUTION,
        image::ColorType::RGBA(8),
    )
    .unwrap();
}
